from nltk.tokenize import word_tokenize
import codecs
from bs4 import BeautifulSoup
import string
from nltk.corpus import stopwords


def get_words_from_web_page(web_page_path):
    f = codecs.open(web_page_path, 'r', 'utf-8')
    web_page_content = f.read()
    bs = BeautifulSoup(web_page_content, 'lxml')

    # remove script elements
    for sc in bs('script'):
        sc.extract()
    for ns in bs('noscript'):
        ns.extract()
    # remove style elements
    for st in bs('style'):
        st.extract()
    # remove link elements
    for l in bs('link'):
        l.extract()
    # remove meta elements
    for m in bs('meta'):
        m.extract()

    text_data = bs.get_text(separator="\n", strip=True)
    # tokenization
    words = word_tokenize(text_data)
    return words


def get_query_words(query):
    slovenian_stop_words = set(stopwords.words("slovenian"))
    # tokenization
    query_words = word_tokenize(query)
    # normalization into lowercase letters
    query_words = [w.lower() for w in query_words]
    # remove punctuations
    query_words = [w for w in query_words if w not in string.punctuation]
    # remove slovenian stop words
    query_words = list(set(query_words).difference(slovenian_stop_words))
    # remove special characters
    query_words = list(set(query_words).difference(set(["...", "\'\'", "·", "–", "--"])))
    return query_words


def get_snippets(web_page_path, indexes, max_snippets):
    words = get_words_from_web_page(web_page_path)
    snippet_indexes = []
    for i in indexes:
        start_index = i - 3
        end_index = i + 3
        if start_index < 0:
            start_index = 0
        if end_index >= len(words):
            end_index = len(words) - 1
        snippet_indexes += list(range(start_index, end_index + 1))
    snippet_indexes = sorted(list(set(snippet_indexes)))

    snippets = []
    snippet = ''
    prev_i = snippet_indexes[0]
    for curr_i in snippet_indexes:
        if abs(prev_i - curr_i) > 1:
            if len(snippets) >= max_snippets - 1:
                break
            else:
                snippets.append(snippet)
                snippet = words[curr_i]
        else:
            snippet += ' ' + words[curr_i]
        prev_i = curr_i
    snippets.append(snippet)
    return snippets