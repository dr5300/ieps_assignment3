import os
from indexer.global_functions import *
from indexer.inverted_index_database import InvertedIndexDatabase
import time


def ii_get_results_for_query(query):
    query_words = get_query_words(query)
    database = InvertedIndexDatabase()
    data = database.get_data_query(query_words)

    results = []
    for row in data:
        web_page_path = row[0]
        document_name = web_page_path.split("/")[-1]
        frequency = row[1]
        indexes = sorted(list(map(int, row[2].split(","))))
        snippets = get_snippets(web_page_path, indexes, 3)
        results.append((frequency, document_name, ' ... '.join(snippets)))
        # take only top 10 results if more found
        if len(results) >= 10:
            break
    return results


def naive_get_results_for_query_in_directory(query_words, directory_path):
    directory = os.fsencode(directory_path)
    slovenian_stop_words = set(stopwords.words("slovenian"))

    results = []
    for document in os.listdir(directory):
        document_name = os.fsdecode(document)
        if document_name.endswith(".html"):
            web_page_path = directory_path + '/' + document_name
            words = get_words_from_web_page(web_page_path)
            # remove punctuations
            filtered_words = [w for w in words if w not in string.punctuation]
            # remove slovenian stop words
            filtered_words = list(set(filtered_words).difference(slovenian_stop_words))
            # remove special characters
            filtered_words = list(set(filtered_words).difference(set(["...", "\'\'", "·", "–", "--"])))

            found_words = list(set(query_words).intersection(filtered_words))
            if found_words:
                indexes = []
                frequency = 0
                for fw in found_words:
                    fw_indexes = [i for i, w in enumerate(words) if fw == w]
                    fw_frequency = len(fw_indexes)
                    indexes += fw_indexes
                    frequency += fw_frequency
                indexes = sorted(indexes)
                snippets = get_snippets(web_page_path, indexes, 3)
                results.append((frequency, document_name, ' ... '.join(snippets)))
    return results


def naive_get_results_for_query(query):
    query_words = get_query_words(query)
    results = []
    results += naive_get_results_for_query_in_directory(query_words, '../data/e-prostor.gov.si')
    results += naive_get_results_for_query_in_directory(query_words, '../data/e-uprava.gov.si')
    results += naive_get_results_for_query_in_directory(query_words, '../data/evem.gov.si')
    results += naive_get_results_for_query_in_directory(query_words, '../data/podatki.gov.si')
    # sort by frequency
    results = sorted(results, key=lambda tup: tup[0])
    # take only top 10 results if more found
    if len(results) >= 10:
        results = results[:10]
    return results


if __name__ == '__main__':
    # query = "predelovalne dejavnosti"
    # query = "trgovina"
    # query = "social services"
    # query = "slovenski jezik"
    # query = "digitalno potrdilo"
    query = "mobilno omrežje"

    ii_start_time = time.time()
    ii_results = ii_get_results_for_query(query)
    ii_end_time = time.time()
    ii_duration_in_ms = int(round((ii_end_time - ii_start_time) * 1000))

    naive_start_time = time.time()
    naive_results = naive_get_results_for_query(query)
    naive_end_time = time.time()
    naive_duration_in_ms = int(round((naive_end_time - naive_start_time) * 1000))

    print("Time comparison for query:", query)
    print()
    print("Query\t\tInverted index time\t\tNaive approach time")
    print()
    print('{}\t\t{}\t\t{}'.format(query, ii_duration_in_ms, naive_duration_in_ms))



