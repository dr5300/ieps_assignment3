import sqlite3


class InvertedIndexDatabase:
    def __init__(self):
        self.connection = sqlite3.connect('inverted-index.db')
        # self.connection.set_trace_callback(print)
        self.cursor = self.connection.cursor()

    def create_tables(self):
        f = open('schema.sql')
        self.cursor.executescript(f.read())
        f.close()
        self.connection.commit()

    def contains_word(self, word):
        self.cursor.execute('SELECT count(*) FROM IndexWord WHERE word=?', (word,))
        self.connection.commit()
        data = self.cursor.fetchone()[0]
        return data != 0

    def add_word(self, word):
        self.cursor.execute('INSERT INTO IndexWord VALUES (?);', (word,))
        self.connection.commit()

    def add_word_info(self, word, document_name, frequency, indexes):
        # join indexes to comma separated string
        indexes = ','.join(map(str, indexes))
        self.cursor.execute(' INSERT INTO Posting VALUES (?, ?, ?, ?);', (word, document_name, frequency, indexes))
        self.connection.commit()

    def get_data_query(self, words):
        if len(words) == 1:
            words = '(\'' + words[0] + '\')'
        else:
            words = str(tuple(words))
        data = self.cursor.execute('SELECT p.documentName AS docName, SUM(frequency) AS freq, GROUP_CONCAT(indexes) AS idxs FROM Posting p WHERE p.word IN {} GROUP BY p.documentName ORDER BY freq DESC;'.format(words))
        self.connection.commit()
        return data
