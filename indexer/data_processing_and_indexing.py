import os
from indexer.inverted_index_database import InvertedIndexDatabase
from indexer.global_functions import *


def add_words_from_web_pages_in_directory_to_db(database, directory_path):
    directory = os.fsencode(directory_path)
    slovenian_stop_words = set(stopwords.words("slovenian"))

    for document in os.listdir(directory):
        document_name = os.fsdecode(document)
        if document_name.endswith(".html"):
            print("Adding words from", document_name, "to database.")
            web_page_path = directory_path + '/' + document_name
            words = get_words_from_web_page(web_page_path)
            # normalization into lowercase letters
            words = [w.lower() for w in words]
            # remove punctuations
            filtered_words = [w for w in words if w not in string.punctuation]
            # remove slovenian stop words
            filtered_words = list(set(filtered_words).difference(slovenian_stop_words))
            # remove special characters
            filtered_words = list(set(filtered_words).difference(set(["...", "\'\'", "·", "–", "--"])))
            # filtered_words = sorted(filtered_words)  # juts for easier debugging

            for fw in filtered_words:
                if not database.contains_word(fw):
                    database.add_word(fw)
                indexes = [i for i, w in enumerate(words) if w == fw]
                database.add_word_info(fw, web_page_path, len(indexes), indexes)


if __name__ == '__main__':

    database = InvertedIndexDatabase()
    database.create_tables()  # remove comment if tables do not exist

    # e-prostor.gov.si
    add_words_from_web_pages_in_directory_to_db(database, '../data/e-prostor.gov.si')
    # e-uprava.gov.si
    add_words_from_web_pages_in_directory_to_db(database, '../data/e-uprava.gov.si')
    # evem.gov.si
    add_words_from_web_pages_in_directory_to_db(database, '../data/evem.gov.si')
    # podatki.gov.si
    add_words_from_web_pages_in_directory_to_db(database, '../data/podatki.gov.si')



