from indexer.global_functions import *
from indexer.inverted_index_database import InvertedIndexDatabase
import time


def get_results_for_query(query):
    query_words = get_query_words(query)
    database = InvertedIndexDatabase()
    data = database.get_data_query(query_words)

    results = []
    for row in data:
        web_page_path = row[0]
        document_name = web_page_path.split("/")[-1]
        frequency = row[1]
        indexes = sorted(list(map(int, row[2].split(","))))
        snippets = get_snippets(web_page_path, indexes, 3)
        results.append((frequency, document_name, ' ... '.join(snippets)))
        # take only top 10 results if more found
        if len(results) >= 10:
            break
    return results


if __name__ == '__main__':
    # query = "predelovalne dejavnosti"
    # query = "trgovina"
    # query = "social services"
    # query = "slovenski jezik"
    # query = "digitalno potrdilo"
    query = "mobilno omrežje"

    start_time = time.time()
    results = get_results_for_query(query)
    end_time = time.time()
    duration_in_ms = int(round((end_time - start_time) * 1000))

    print("Results for query:", query)
    print()
    print("Results found in", duration_in_ms, "ms")
    print()
    print("Frequencies\t\tDocument\t\tSnippet")
    print()
    for r in results:
        print(r[0], r[1], r[2], sep="\t\t")




