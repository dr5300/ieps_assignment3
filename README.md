# IEPS - Programming assignment 3

## Installation
Use python prompt to open nltk package downloader:
```bash
import nltk
nltk.download()
```
Download the following nltk packages:
* Corpora > stopwords
* Models > punkt

Add slovenian stopwords file to stopwords directory:
* C:\nltk_data\corpora\stopwords
* C:\Users\username\AppData\Roaming\nltk_data\corpora

Use *pip* to install required packages:

```bash
pip install -r requirements.txt
```

## Database
Database *inverted-index.db* can be found in *indexer* folder.

### Contributors ###
* **Denis Rajković**
* **Matic Bizjak**